from bs4 import BeautifulSoup
import requests
from googlesearch import search
from fuzzywuzzy import fuzz
import re
import os
import sys
import pyperclip
import time

print('\nNate Gould\'s')
print('''   ____        _       ____            _            
  / __ \      (_)     |  _ \          | |           
 | |  | |_   _ _ ____ | |_) |_   _ ___| |_ ___ _ __ 
 | |  | | | | | |_  / |  _ <| | | / __| __/ _ \ '__|
 | |__| | |_| | |/ /  | |_) | |_| \__ \ ||  __/ |   
  \___\_\\__,_|_/___|  |____/ \__,_|___/\__\___|_|  ''')


# my_parser = argparse.ArgumentParser(description="user can choose between file mode (default) vs covert mode (-c)")
# my_parser.add_argument("-c")
# my_parser.parse_args()


def get_answer(question, answers):
    search_question = "liberty quizlet " + question
    print(question)
    for url in search(search_question, tld="co.in", num=10, stop=10, pause=2):
        print('URL: ' + url)
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"}
        if 'quizlet' not in url:
            continue
        try:
            page = requests.get(url, headers=headers)
        except:
            continue
        if page.status_code != 200:
            continue
        soup = BeautifulSoup(page.text, 'html.parser')

        span_text = soup.find_all('span', class_="TermText notranslate lang-en")
        flag = False

        for i in span_text:
            if flag == True:
                line = i.get_text()
                answers.update({question: line})
                break

            line = i.get_text()
            if fuzz.ratio(line.lower(),
                          question.lower()) >= 90:  # if levenstein ratio between the question that the user entered and the current line on quizlet is >= 90, change flag to true so that during the next loop iteration this program knows its the answer to this question
                flag = True
    if not answers:
        print("No answers found")
    else:
        show_answers(answers)


def show_answers(answers):
    for q, a in answers.items():
        print()
        print("Question: " + q)
        print("Answer: " + a)
        print()

def process_file():
    answers = {}
    print('\nChecking for data in input file...\n')
    # if input file is empty...
    if os.stat("input.txt").st_size == 0:
        print("input.txt is empty. Please put copy/paste questions section from your quiz.")
    else:
        with open('input.txt', encoding="utf8") as file:
            contents = file.read()
            # if script detects a canvas quiz...
            if re.findall('Question .*\d*\.?\d+ pts\n.*\nGroup of answer choices', contents):
                raw_questions = re.findall('Question .*\d*\.?\d+ pts\n.*\nGroup of answer choices', contents)
                for question in raw_questions:
                    get_answer(question.split("\n")[1], answers)
                show_answers(answers)
            # if script detects a blackboard quiz...
            elif re.findall("QUESTION \\d+\n+(.*)", contents):
                questions = re.findall("QUESTION \\d+\n+(.*)", contents)
                for question in questions:
                    get_answer(question)
                show_answers(answers)
            else:
                print("Error. Input file doesn't match any known format.")

def covert_mode():
    print('#' * 30)
    print('This program waits for questions to be copied to the clipboard and then searches for the answer')
    print('Simply highlight your question, press ctrl+C, and the program will get to work')
    print('Press CTR-C at any time to exit')
    print('#' * 30)

    pyperclip.copy('')
    recent_value = ''
    while True:
        answers = {}
        user_input = pyperclip.paste()
        if user_input != recent_value:
            print("Processing...")
            get_answer(user_input, answers)
            pyperclip.copy('')
        time.sleep(0.1)

if len(sys.argv) == 2:
    if sys.argv[1].lower() in ["-c","c","covert"]:
        covert_mode()
    else:
        print("Error: " + sys.argv[1] + " is an invalid argument")
else:
    process_file()

# example english question
# The boys left town; in addition, they took the money with them.

#another problem question: The Checked property of a RadioButton can hold the values ________________________ .


